const autoprefixer = require('autoprefixer');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');
const globImporter = require('node-sass-glob-importer');

module.exports = (env, argv) => {
	function isDevelopment() {
		return argv.mode === 'development';
	}

	let config = {
		entry: {
			bundle: './assets/js/index.js'
		},
		output: {
			filename: '[name].js'
		},
		optimization: {
			minimizer: [
				new TerserPlugin({
					sourceMap: true
				}),
				new OptimizeCSSAssetsPlugin(
					{
						cssProcessorOptions: {
							map: {
								inline: false,
								annotation: true
							}
						}
					})
			]
		},
		plugins: [
			new CleanPlugin(),
			new MiniCSSExtractPlugin({
				chunkFilename: '[id].css',
				filename: (chunkData) => {
					return chunkData.chunk.name === 'bundle' ? 'style.css' : '[name].css'
				}
			})
		],
		devtool: isDevelopment() ? 'cheap-module-source-map' : 'source-map',
		module: {
			rules: [
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: [
								'@babel/preset-env',
								[
									'@babel/preset-react',
									{
										"pragma": "wp.element.createElement",
										"pragmaFrag": "wp.element.Fragment",
										"development": isDevelopment()
									}
								]
							]
						}
					}
				},
				{
					test: /\.(sa|sc|c)ss$/,
					use: [
						MiniCSSExtractPlugin.loader,
						{
							loader: 'css-loader',
							options: {
								url: false
							}
						},
						{
							loader: 'postcss-loader',
							options: {
								plugins: [
									autoprefixer()
								]
							}
						},
						{
							loader: 'sass-loader',
							options: {
								importer: globImporter(),
								// Prefer `dart-sass`
								implementation: require('sass'),
							},
						}
					]
				}
			]
		},
		externals: {
			jquery: 'jQuery',
			swiper: 'Swiper',
			'@wordpress/blocks': ['wp', 'blocks'],
			'@wordpress/i18n': ['wp', 'i18n'],
			'@wordpress/components': ['wp', 'components'],
			'@wordpress/editor': ['wp', 'editor']
		}
	};
	return config;
};