<?php

namespace Digikogu;

class Admin {
	private $plugin_name;
	private $version;
	private $rest_namespace;

	public function __construct( $plugin_name, $version, $rest_namespace ) {
		$this->plugin_name    = $plugin_name;
		$this->version        = $version;
		$this->rest_namespace = $rest_namespace;
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 */
	public function enqueue_scripts() {
		wp_enqueue_script(
			$this->plugin_name . '-script',
			plugin_dir_url( __DIR__ ) . 'dist/bundle.js',
			array( 'jquery' ),
			$this->version,
			true
		);


		wp_localize_script(
			$this->plugin_name . '-script',
			'digikogu_object',
			array(
				'ajax_url'       => admin_url( 'admin-ajax.php' ),
				'plugin_path'    => plugin_dir_url( __DIR__ ),
				'rest_namespace' => $this->rest_namespace,
				'site_url'       => get_site_url(),
			)
		);
	}
}