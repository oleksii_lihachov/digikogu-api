<?php

/**
 * The file that defines the core plugin class
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 */

namespace Digikogu;

class Main {
	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 */
	protected $version;

	private $logs;
	private $rest_namespace = 'digikogu/v1';
	private $api_url = 'https://digikogu.ekm.ee/reproprint/_ajax-json';

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 */
	public function __construct() {
		if ( defined( 'DIGIKOGU_VERSION' ) ) {
			$this->version = DIGIKOGU_VERSION;
		} else {
			$this->version = '1.0';
		}

		$this->plugin_name = DIGIKOGU_PLUGIN_SLUG;
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	private function load_dependencies() {
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Admin.php';

		$this->loader = new Loader();

		$wc_integration = new WC_Integration();
		$this->loader->add_action( 'plugins_loaded', $wc_integration, 'init', 10, 0 );

		$tab = new OptionTab();
		$this->loader->add_action( 'woocommerce_product_write_panel_tabs', $tab, 'add_panel_tab', 10, 0 );
		$this->loader->add_action( 'woocommerce_product_data_panels', $tab, 'panel_tab_content', 10, 0 );
		$this->loader->add_action( 'woocommerce_process_product_meta', $tab, 'update_admin_fields', 10, 1 );


		$logs       = new Logs();
		$this->logs = $logs;
		new LogsPage( $logs );

		$rest = new Rest( $this->rest_namespace, $this->api_url, $logs );
		$this->loader->add_action( 'rest_api_init', $rest, 'action_rest_api_init_trait', 10, 0 );
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the spsr_Landing_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 */
	private function set_locale() {
		$plugin_i18n = new i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 */
	private function define_admin_hooks() {
		$plugin_admin = new Admin( $this->plugin_name, $this->version, $this->rest_namespace );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 */
	private function define_public_hooks() {
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 */
	public function run() {
		$this->loader->run();
	}
}