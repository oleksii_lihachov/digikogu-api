<?php

namespace Digikogu;

class Activation {
	public function __construct() {
		if ( ! class_exists( 'WooCommerce' ) ) {
			die( 'WooCommerce plugin not activated!' );
		}
	}
}