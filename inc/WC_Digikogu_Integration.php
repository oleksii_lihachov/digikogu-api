<?php

namespace Digikogu;

use WC_Integration;

class WC_Digikogu_Integration extends WC_Integration {
	/**
	 * Init and hook in the integration.
	 */
	public function __construct() {
		$this->id           = 'digikogu-integration';
		$this->method_title = 'Digikogu integration';

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

		// Actions.
		add_action( 'woocommerce_update_options_integration_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_update_options_integration_' . $this->id, array( $this, 'process_admin_custom_options' ) );
	}

	public function admin_options() {
		parent::admin_options();
		$categories    = get_terms( array(
			'taxonomy' => 'product_cat',
		) );
		$digikogu_cats = get_option( 'digikogu_cats' );
		?>
		<div class="wrap">
			<table class="form-table">
				<tr>
					<th><?php esc_html_e( 'Categories', DIGIKOGU_TEXT_DOMAIN ); ?></th>
					<td>
						<select name="digikogu_cats[]" id="digikogu_cats" multiple>
							<?php
							foreach ( $categories as $category ) {
								$selected = in_array( $category->term_id, $digikogu_cats ) ? 'selected' : '';
								echo '<option value="' . $category->term_id . '" ' . $selected . '>' . $category->name . '</option>';
							}
							?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<?php
	}

	public function process_admin_custom_options() {
		if ( isset( $_POST['digikogu_cats'] ) ) {
			update_option( 'digikogu_cats', $_POST['digikogu_cats'] );
		}
	}
}