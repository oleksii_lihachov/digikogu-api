<?php

namespace Digikogu;

use WP_List_Table;

class LogsTable extends WP_List_Table {
	private $order;
	private $orderby;
	private $posts_per_page = 20;
	private $logs;

	public function __construct( Logs $logs ) {
		parent:: __construct( array(
			'singular' => 'digikogu api Log',
			'plural'   => 'digikogu api logs',
			'ajax'     => true
		) );
		$this->logs = $logs;
		$this->set_order();
		$this->set_orderby();
		$this->prepare_items();
		$this->display();
	}

	private function get_sql_results() {
		$sql_select = "SELECT * FROM {$this->logs->table_name} ORDER BY {$this->orderby} {$this->order}";
		return $this->logs->wpdb->get_results( $sql_select );
	}

	public function set_order() {
		$order = 'DESC';

		if ( isset( $_GET['order'] ) AND $_GET['order'] ) {
			$order = $_GET['order'];
		}

		$this->order = esc_sql( $order );
	}

	public function set_orderby() {
		$order_by = 'date';

		if ( isset( $_GET['orderby'] ) AND $_GET['orderby'] ) {
			$order_by = $_GET['orderby'];
		}

		$this->orderby = esc_sql( $order_by );
	}

	/**
	 * @see WP_List_Table::ajax_user_can()
	 */
	public function ajax_user_can() {
		return current_user_can( 'edit_posts' );
	}

	/**
	 * @see WP_List_Table::no_items()
	 */
	public function no_items() {
		_e( 'No Logs found.', DIGIKOGU_TEXT_DOMAIN );
	}

	/**
	 * @see WP_List_Table::get_views()
	 */
	public function get_views() {
		return array();
	}

	/**
	 * @see WP_List_Table::get_columns()
	 */
	public function get_columns() {
		return array(
			'log_id'     => __( 'ID', DIGIKOGU_TEXT_DOMAIN ),
			'product_id' => __( 'Product ID', DIGIKOGU_TEXT_DOMAIN ),
			'product'    => __( 'Product Name', DIGIKOGU_TEXT_DOMAIN ),
			'ekm_number' => __( 'EMK Number', DIGIKOGU_TEXT_DOMAIN ),
			'status'     => __( 'Status', DIGIKOGU_TEXT_DOMAIN ),
			'date'       => __( 'Date', DIGIKOGU_TEXT_DOMAIN ),
		);
	}

	/**
	 * @see WP_List_Table::get_sortable_columns()
	 */
	public function get_sortable_columns() {
		return array(
			'log_id'     => array( 'log_id', true ),
			'product_id' => array( 'product_id', true ),
			'product'    => array( 'product', true ),
			'date'       => array( 'date', true ),
		);
	}

	/**
	 * Prepare data for display
	 * @see WP_List_Table::prepare_items()
	 */
	public function prepare_items() {
		$columns               = $this->get_columns();
		$hidden                = array();
		$sortable              = $this->get_sortable_columns();
		$this->_column_headers = array(
			$columns,
			$hidden,
			$sortable
		);

		// SQL results
		$logs = $this->get_sql_results();
		empty( $logs ) AND $logs = array();

		# >>>> Pagination
		$per_page     = $this->posts_per_page;
		$current_page = $this->get_pagenum();
		$total_items  = count( $logs );
		$this->set_pagination_args( array(
			'total_items' => $total_items,
			'per_page'    => $per_page,
			'total_pages' => ceil( $total_items / $per_page )
		) );
		$last_post  = $current_page * $per_page;
		$first_post = $last_post - $per_page + 1;
		$last_post > $total_items AND $last_post = $total_items;

		// Setup the range of keys/indizes that contain
		// the posts on the currently displayed page(d).
		// Flip keys with values as the range outputs the range in the values.
		$range = array_flip( range( $first_post - 1, $last_post - 1, 1 ) );

		// Filter out the posts we're not displaying on the current page.
		$logs_array = array_intersect_key( $logs, $range );
		# <<<< Pagination

		// Prepare the data
		$this->items = $logs_array;
	}

	/**
	 * A single column
	 */
	public function column_default( $item, $column_name ) {
		return $item->$column_name;
	}

	/**
	 * Override of table nav to avoid breaking with bulk actions & according nonce field
	 */
	public function display_tablenav( $which ) {
		?>
		<div class="tablenav <?php echo esc_attr( $which ); ?>">
			<!--
            <div class="alignleft actions">
                <?php # $this->bulk_actions( $which ); ?>
            </div>
             -->
			<?php
			$this->extra_tablenav( $which );
			$this->pagination( $which );
			?>
			<br class="clear"/>
		</div>
		<?php
	}

	/**
	 * Disables the views for 'side' context as there's not enough free space in the UI
	 * Only displays them on screen/browser refresh. Else we'd have to do this via an AJAX DB update.
	 *
	 * @see WP_List_Table::extra_tablenav()
	 */
	public function extra_tablenav( $which ) {
		global $wp_meta_boxes;
		$views = $this->get_views();
		if ( empty( $views ) )
			return;

		$this->views();
	}
}