<?php

namespace Digikogu;

use WP_REST_Server;
use WP_REST_Request;

class Rest {
	private $rest_namespace;
	private $logs;
	private $api_url;

	public function __construct( $namespace, $api_url, Logs $logs ) {
		$this->logs           = $logs;
		$this->rest_namespace = $namespace;
		$this->api_url        = $api_url;
	}

	public function action_rest_api_init_trait() {
		register_rest_route(
			$this->rest_namespace,
			'/log/',
			array(
				'methods'             => WP_REST_Server::CREATABLE,
				'permission_callback' => function ( WP_REST_Request $request ) {
					return true;
				},
				'callback'            => array( $this, 'rest_log_handler' ),
			) );

		register_rest_route(
			$this->rest_namespace,
			'/download/',
			array(
				'methods'             => WP_REST_Server::READABLE,
				'permission_callback' => function ( WP_REST_Request $request ) {
					return true;
				},
				'callback'            => array( $this, 'rest_download_handler' ),
			) );
	}

	public function rest_log_handler( WP_REST_Request $request ) {
		$body_json = $request->get_body();
		$body      = json_decode( $body_json, true );
		$product   = wc_get_product( $body['id'] );

		$this->logs->insert(
			$body['id'], $product->get_title(),
			$body['ekm_number'],
			$body['status']
		);
	}

	public function rest_download_handler( WP_REST_Request $request ) {
		$ekm_number = $request->get_params()['ekm_nr'];
		$post_id    = $request->get_params()['post_id'];

		$request_data = array(
			'ekm_nr' => $ekm_number
		);
		$query        = http_build_query( $request_data );

		$ch = curl_init( $this->api_url . '?' . $query );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		$curl_response = curl_exec( $ch );
		curl_close( $ch );

		if ( $curl_response === false ) {
			wp_send_json_error(
				array(
					'message' => 'Curl error: ' . curl_error( $ch ),
				),
				400
			);
		}

		$connect_response = json_decode( $curl_response, true );

		if ( empty( $connect_response ) ) {
			wp_send_json_error(
				array(
					'message' => __( 'Invalid EKM number', DIGIKOGU_TEXT_DOMAIN ),
				),
				400
			);
		}


		$controller = new Controller( wc_get_product( $post_id ), $connect_response );
		$controller->sync();
		$sync_errors = $controller->get_errors();

		if ( $sync_errors ) {
			wp_send_json_error(
				array(
					'message' => $sync_errors,
				),
				400
			);
		}

		wp_send_json_success();
	}
}