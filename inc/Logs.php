<?php

namespace Digikogu;

class Logs {
	public $wpdb;
	public $table_name;

	public function __construct() {
		global $wpdb;
		$this->wpdb       = &$wpdb;
		$this->table_name = $wpdb->prefix . "digikogu_logs";

		$this->init();
	}

	private function init() {
		$charset_collate    = $this->wpdb->get_charset_collate();
		$search_table_query = $this->wpdb->prepare( 'SHOW TABLES LIKE %s', $this->wpdb->esc_like( $this->table_name ) );

		if ( $this->wpdb->get_var( $search_table_query ) === $this->table_name ) {
			return false;
		}

		$sql = "CREATE TABLE {$this->table_name} (
			   `log_id` INT NOT NULL AUTO_INCREMENT,
				`product_id` INT NOT NULL,
			    `product` VARCHAR(255) NOT NULL,
			    `ekm_number` VARCHAR(255) NOT NULL,
  			    `status` VARCHAR(255) NULL,
  		  	    `date` DATETIME NOT NULL DEFAULT now(),
  				 UNIQUE INDEX `log_id_UNIQUE` (`log_id` ASC),
  				 PRIMARY KEY (`log_id`)) {$charset_collate};";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		return true;
	}

	public function insert( $id, $product_name, $ekm_number, $status ) {
		$this->wpdb->insert(
			$this->table_name,
			array(
				'product_id' => $id,
				'product'    => $product_name,
				'ekm_number' => $ekm_number,
				'status'     => $status,
				'date'       => date( 'Y-m-d H:i:s' ),
			)
		);
	}

	public function clear() {
		$this->wpdb->query( "TRUNCATE {$this->table_name}" );
	}
}