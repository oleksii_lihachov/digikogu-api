<?php

namespace Digikogu;

class WC_Integration {

	/**
	 * Initialize the plugin.
	 */
	public function init() {
		// Checks if WooCommerce is installed.
		if ( class_exists( 'WC_Integration' ) ) {
			new WC_Digikogu_Integration();

			// Register the integration.
			add_filter( 'woocommerce_integrations', array( $this, 'add_integration' ) );
		} else {
			// throw an admin error if you like
		}
	}

	/**
	 * Add a new integration to WooCommerce.
	 */
	public function add_integration( $integrations ) {
		$integrations[] = 'Digikogu\WC_Digikogu_Integration';
		return $integrations;
	}
}