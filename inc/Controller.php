<?php

namespace Digikogu;

use WC_Product;

class Controller {
	private $product;
	private $data;
	private $error;

	public function __construct( WC_Product $product, $data ) {
		$this->product = $product;
		$this->data    = $data;
	}

	public function sync() {
		$this->process_attributes();
		$this->process_image();
		$this->process_categories();
		$this->product->update_meta_data( 'ekm_number', $this->data['ekm_nr'] );
		$this->product->set_name( $this->data['name_eng'] );
		$this->product->save();

	}

	public function get_errors() {
		return $this->error;
	}

	private function process_categories() {
		$cats = $this->data['categories'];

		if ( empty( $cats ) ) {
			return;
		}

		foreach ( $cats as $cat ) {
			if ( ! term_exists( $cat, 'product_cat' ) ) {
				wp_insert_term( $cat, 'product_cat' ); // Create the term
			}

			wp_set_object_terms( $this->product->get_id(), $cat, 'product_cat', true );
		}
	}

	private function process_image() {
		if ( empty( $this->data['image'] ) ) {
			return;
		}

		$basename      = pathinfo( $this->data['image'] )['basename'];
		$file_title    = pathinfo( $this->data['image'] )['filename'];
		$wp_upload_dir = wp_upload_dir();
		$file_name     = trailingslashit( $wp_upload_dir["path"] ) . $basename;
		$contents      = file_get_contents( $this->data['image'] );

		// Upload file
		$file_written = file_put_contents( $file_name, $contents );

		if ( $file_written ) {
			$wp_filetype = wp_check_filetype( $file_name, null );

			$post_attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_title'     => $file_title,
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			$attach_id = wp_insert_attachment( $post_attachment, $file_name );

			require_once( trailingslashit( ABSPATH ) . 'wp-admin/includes/file.php' );
			require_once( trailingslashit( ABSPATH ) . 'wp-admin/includes/image.php' );
			require_once( trailingslashit( ABSPATH ) . 'wp-admin/includes/media.php' );

			$attach_data = wp_generate_attachment_metadata( $attach_id, $file_name );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			$this->product->set_image_id( $attach_id );
			$this->product->save();
		} else {
			$this->error = __( 'Could not upload image', DIGIKOGU_TEXT_DOMAIN );
			return;
		}
	}

	private function process_attributes() {
		$attrs         = $this->get_raw_attributes();
		$product_attrs = array();

		foreach ( $attrs as $slug => $terms ) {
			$taxonomy_slug    = wc_attribute_taxonomy_name( $slug );
			$taxonomy_details = get_taxonomy( $taxonomy_slug );

			// Create taxonomy
			if ( ! $taxonomy_details ) {
				wc_create_attribute( array(
					'name'         => strtoupper( $taxonomy_slug ),
					'slug'         => $taxonomy_slug,
					'type'         => 'text',
					'order_by'     => 'menu_order',
					'has_archives' => true,
				) );

				register_taxonomy(
					$taxonomy_slug,
					apply_filters( 'woocommerce_taxonomy_objects_' . $taxonomy_slug, array( 'product' ) ),
					apply_filters( 'woocommerce_taxonomy_args_' . $taxonomy_slug, array(
						'labels'       => array(
							'name' => strtoupper( $taxonomy_slug ),
						),
						'hierarchical' => true,
						'show_ui'      => false,
						'query_var'    => true,
						'rewrite'      => false,
					) )
				);

				//Clear caches
				delete_transient( 'wc_attribute_taxonomies' );
			}

			if ( is_array( $terms ) ) {
				foreach ( $terms as $term ) {
					$this->process_term( $term, $taxonomy_slug );
				}
			} else {
				$this->process_term( $terms, $taxonomy_slug );
			}

			// Update product attributes
			$product_attrs[ $taxonomy_slug ] = array(
				'name'         => $taxonomy_slug,
				'value'        => '',
				'position'     => '',
				'is_visible'   => 0,
				'is_variation' => 1,
				'is_taxonomy'  => 1
			);
		}

		update_post_meta( $this->product->get_id(), '_product_attributes', $product_attrs );
	}

	private function process_term( $term, $taxonomy_slug ) {
		// Check if the Term name exist and if not we create it.
		if ( ! term_exists( $term, $taxonomy_slug ) ) {
			wp_insert_term( $term, $taxonomy_slug ); // Create the term
		}

		// Get the post Terms names from the parent variable product.
		$post_term_names = wp_get_post_terms( $this->product->get_id(), $taxonomy_slug, array( 'fields' => 'names' ) );

		// Check if the post term exist and if not we set it in the parent variable product.
		if ( ! in_array( $term, $post_term_names ) ) {
			wp_set_post_terms( $this->product->get_id(), $term, $taxonomy_slug, true );
		}

		$term_slug = get_term_by( 'name', $term, $taxonomy_slug )->slug; // Get the term slug
		update_post_meta( $this->product->get_id(), 'attribute_' . $taxonomy_slug, $term_slug );
	}

	private function get_raw_attributes() {
		$attrs          = array();
		$expected_attrs = array(
			'kunstnik'      => array(
				'key'      => 'author_name',
				'multiple' => false
			),
			'laius'         => array(
				'key'      => 'width',
				'multiple' => false
			),
			'ekmnr'         => array(
				'key'      => 'ekm_nr',
				'multiple' => false
			),
			'materjal_est'  => array(
				'key'      => 'materials_est',
				'multiple' => true
			),
			'keywords_est'  => array(
				'key'      => 'keywords_est',
				'multiple' => true
			),
			'dateering'     => array(
				'key'      => 'created_year',
				'multiple' => false
			),
			'digikogu_id'   => array(
				'key'      => 'id',
				'multiple' => false
			),
			'name_eng'      => array(
				'key'      => 'name_eng',
				'multiple' => false
			),
			'materials_eng' => array(
				'key'      => 'materials_eng',
				'multiple' => true
			),
			'technique_eng' => array(
				'key'      => 'technique_eng',
				'multiple' => true
			),
			'haru'          => array(
				'key'      => 'branch',
				'multiple' => false
			),
			'korgus'        => array(
				'key'      => 'height',
				'multiple' => false
			),
			'tehnika'       => array(
				'key'      => 'technique_est',
				'multiple' => false
			),
			'name_est'      => array(
				'key'      => 'name_est',
				'multiple' => false
			),
			'keywords_eng'  => array(
				'key'      => 'keywords_eng',
				'multiple' => true
			),
		);

		foreach ( $expected_attrs as $slug => $info ) {
			if ( array_key_exists( $info['key'], $this->data ) && ! empty( $this->data[ $info['key'] ] ) ) {
				$attrs[ $slug ] = $info['multiple'] ? explode( ', ', $this->data[ $info['key'] ] ) : $this->data[ $info['key'] ];
			}
		}

		return $attrs;
	}
}