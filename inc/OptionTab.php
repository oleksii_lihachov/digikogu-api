<?php

namespace Digikogu;

class OptionTab {
	public function add_panel_tab() {
		global $post;
		$product_cats  = wp_get_post_terms( $post->ID, 'product_cat' );
		$digikogu_cats = get_option( 'digikogu_cats' );
		$is_access     = false;

		foreach ( $product_cats as $cat ) {

			if ( in_array( strval( $cat->term_id ), $digikogu_cats ) ) {
				$is_access = true;
				break;
			}
		}


		if ( ! $is_access ) {
			return;
		}

		echo '<li class="digikogu_product_tab"><a href="#digikogu_product_data"> ' . __( 'Digikogu API', DIGIKOGU_TEXT_DOMAIN ) . '</a></li>';
	}

	public function panel_tab_content() {
		global $post;
		$value = get_post_meta( $post->ID, 'ekm_number', true );
		?>
		<div id="digikogu_product_data" class="panel woocommerce_options_panel wc-metaboxes-wrapper">
			<div class="options_group">
				<p class="form-field">
					<label for="ekm_number"><?php esc_html_e( 'EKM number', DIGIKOGU_TEXT_DOMAIN ); ?></label>
					<input type="text"
						   id="ekm_number"
						   name="ekm_number"
						   class="short"
						   value="<?php echo esc_attr( $value ); ?>"/>
				</p>
			</div>
			<div class="toolbar">
				<button type="button"
						id="ekm_action"
						data-id="<?php echo esc_attr( $post->ID ); ?>"
						class="button button-primary">
					<?php esc_html_e( 'Download Digikogu data', DIGIKOGU_TEXT_DOMAIN ); ?>
				</button>
			</div>
		</div>
		<?php
	}

	public function update_admin_fields( $id ) {
		$product    = wc_get_product( $id );
		$ekm_number = isset( $_POST['ekm_number'] ) ? $_POST['ekm_number'] : '';
		$product->update_meta_data( 'ekm_number', $ekm_number );
		$product->save();
	}
}