export default jQuery(document).ready(function ($) {
  const $button = $('#ekm_action');

  if ($button.length) {
    const logsUrl = `${window.digikogu_object.site_url}/wp-json/${window.digikogu_object.rest_namespace}/log/`;
    const downloadUrl = `${window.digikogu_object.site_url}/wp-json/${window.digikogu_object.rest_namespace}/download/`;

    const log = (id, ekm_number, status) => {
      return fetch(
        logsUrl,
        {
          method: 'POST',
          body: JSON.stringify({
            id,
            ekm_number,
            status
          })
        }
      );
    };

    const showNotice = (type, message, action = {}) => {

      wp.data.dispatch('core/notices').createNotice(
        type, // Can be one of: success, info, warning, error.
        message, // Text string to display.
        {
          isDismissible: true, // Whether the user can dismiss the notice.,
          actions: [
            action
          ]
        }
      );
    };

    $button.on('click', function (ev) {
      ev.preventDefault();
      ev.stopPropagation();

      const value = $('#ekm_number').val();
      const postId = $(this).data('id');

      if (!value) {
        showNotice('error', 'Empty EMK number');
        return;
      }

      fetch(
        `${downloadUrl}?ekm_nr=${value}&&post_id=${postId}`
      )
        .then(response => response.json())
        .then(result => {
          if (!result.success) {
            showNotice('error', result.data.message);
            return log(postId, value, result.data.message);
          } else {
            showNotice('success', 'Success! Please refresh a page', {
              url: window.location.href,
              label: 'Refresh',
            });
            return log(postId, value, 'Success fetching');
          }
        })
        .catch(e => {
          showNotice('error', e.message);
          return log(postId, value, e.message);
        });
    });
  }
});