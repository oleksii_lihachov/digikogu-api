<?php

/**
 * Plugin Name: Digikogu API
 * Plugin URI: http://www.ids.ee/
 * Version: 1.1
 * Author: iDS
 * Author URI: http://www.ids.ee
 * Text Domain: digikogu-api
 */

namespace Digikogu;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


// ======================  CONSTANTS ===================================
define( 'DIGIKOGU_VERSION', '1.0' );
define( 'DIGIKOGU_PLUGIN_SLUG', 'digikogu-api' );
define( 'DIGIKOGU_TEXT_DOMAIN', 'digikogu-api' );
define( 'DIGIKOGU_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'DIGIKOGU_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require_once plugin_dir_path( __FILE__ ) . 'autoloader.php';
$loader = new \Psr4AutoloaderClass();
$loader->register();
$loader->addNamespace( 'Digikogu', __DIR__ . '/inc' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/Main.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/Activation.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_digikogu_api() {
	$plugin = new Main();
	$plugin->run();
}


try {
	run_digikogu_api();
} catch ( \Exception $e ) {
	new \WP_Error( $e );
}

/**
 * The code that runs during plugin activation.
 */
register_activation_hook( __FILE__, function () {
	flush_rewrite_rules();
	new Activation();
} );

/**
 * The code that runs during plugin deactivation.
 */
register_deactivation_hook( __FILE__, function () {
	flush_rewrite_rules();
} );